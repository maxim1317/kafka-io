#!/bin/sh

mkinit --inplace --noattrs kafkaio/ > /dev/null
mkinit --inplace --noattrs kafkaio/src > /dev/null
mkinit --inplace --noattrs kafkaio/tests > /dev/null