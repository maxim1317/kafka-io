kafka_python==1.4.6
pytest==5.0.0
jsonschema==3.0.1
colorlog==4.0.2
kafka==1.3.5
