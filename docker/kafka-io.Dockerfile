FROM python:3-alpine

RUN apk add\
    bash

RUN pip3 install --no-cache \
    kafka-python \
    pytest \
    pytest-cov \
    pytest-sugar \
    coverage-badge \
    docstr-coverage \
    jsonschema \
    mkinit \
    colorlog

WORKDIR /kafkaio

COPY kafkaio/ kafkaio/
COPY prepare.sh .
COPY run_tests.sh .
COPY run.sh .

CMD bash run.sh