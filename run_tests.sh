#! /bin/bash

docstr-coverage --skipmagic --verbose=3 kafkaio/ && \
pytest --cov=kafkaio kafkaio/tests/ && coverage html