from kafkaio.tests import test_kafkaIO
from kafkaio.tests import test_kafka_i

__all__ = ['test_kafkaIO', 'test_kafka_i']
