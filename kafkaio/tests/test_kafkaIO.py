import pytest
from ..src.kafkaIO import Kafka


def test_Kafka_connect_wrong_input():

    kafka = Kafka()
    with pytest.raises(TypeError):
        kafka.connect()
    with pytest.raises(TypeError):
        kafka.connect(server='10.1.0.21')
    with pytest.raises(TypeError):
        kafka.connect(server='10.1.0.21', topic_in='test')
    with pytest.raises(TypeError):
        kafka.connect(server='10.1.0.21', group_in='test')
    with pytest.raises(TypeError):
        kafka.connect(server='10.1.0.21', topic_out='test', group_in='test')

    with pytest.raises(ValueError):
        kafka.connect(server='1.1', topic_in='test', group_in='test', topic_out='test')
    with pytest.raises(ValueError):
        kafka.connect(server='1.1.2.290', topic_in='test', group_in='test', topic_out='test')


def test_Kafka_connect_for_input():

    kafka = Kafka()
    kafka.connect(server='10.1.0.21:29219219', topic_in='test', group_in='test')
    assert kafka.ready() is False
    kafka.connect(server='10.1.0.21', topic_out='test')
    assert kafka.ready() is True
    kafka.connect(server='10.1.0.21', topic_in='test', group_in='test', topic_out='test')
    assert kafka.ready() is True


def test_double_connect():

    kafka = Kafka()
    kafka.connect(server='10.1.0.21', topic_in='test', group_in='test', topic_out='test')
    kafka.connect(server='10.1.0.21', topic_in='test', group_in='test', topic_out='test')
    assert kafka.ready() is True


def test_no_connection():
    from kafka.errors import NoBrokersAvailable

    kafka = Kafka()
    with pytest.raises(NoBrokersAvailable):
        kafka.connect(server='10.0.0.230', topic_in='test', group_in='test', topic_out='test')


def test_pull_no_connection():

    kafka = Kafka()
    with pytest.raises(Exception):
        kafka.pull()


def test_push_no_connection():

    kafka = Kafka()
    with pytest.raises(Exception):
        kafka.push(message={'help': 'help'})
