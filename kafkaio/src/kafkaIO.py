"""
    Для работы с Kafka

    Как пользоваться:
    from .kafkaIO import Kafka

    self.kafka = Kafka()  # Инициализация
    self.kafka.connect(
        server,     # адрес Кафки
        topic_in,   # топик, из которого получаются изображения
        group_in,   # группа, к которой принадлежит консьюмер (просто любое имя)
        topic_out   # топик, куда кладутся боксы с предиктами
    )

    message = self.kafka.pull(offset)   # получить картинку, offset не обязателен
    prediction = self._predict(message) # просто отдать MLS на обработку
    self.kafka.push(prediction)         # отправить предикты
"""

import time

from .kafkaI import KafkaIn
from .kafkaO import KafkaOut
from .utils import setup_logger


logger = setup_logger()


class Kafka(object):
    """Sets Kafka IO"""

    def __init__(self):
        self.kafka_in  = KafkaIn()
        self.kafka_out = KafkaOut()

    def connect(self, server, port=9092, topic_in=None, group_in=None, topic_out=None):
        logger.debug('Connecting to ' + server + ': STARTED')

        if all(i is None for i in [topic_in, group_in, topic_out]):
            logger.debug('Connecting to ' + server + ': FAILED')
            error_msg = 'Input topic, consumer group name and/or output topic are expected'
            logger.error(error_msg)
            raise TypeError(error_msg)

        if None not in [topic_in, group_in]:
            self.connect_in(
                server=server,
                port=port,
                topic=topic_in,
                group=group_in
            )
        elif (topic_in is None) ^ (group_in is None):
            logger.debug('Connecting to ' + server + ': FAILED')
            error_msg = 'Input topic and consumer group name are expected'
            logger.error(error_msg)
            raise TypeError(error_msg)

        if topic_out is not None:
            self.connect_out(
                server=server,
                port=port,
                topic=topic_out
            )

        logger.debug('Connecting to ' + server + ': SUCCESS')
        return

    def connect_in(self, server, topic, group, port=9092):
        from kafka.errors import NoBrokersAvailable

        tries = 1

        while tries <= 5:
            message = 'Connecting to topic {} at "{}:{}" as consumer in {} group. Try #{}...'.format(
                topic,
                server,
                port,
                group,
                tries
            )
            logger.debug(message)

            try:
                self.kafka_in.connect(
                    server=server,
                    topic=topic,
                    group=group,
                    port=port
                )
                message = 'Connection to topic {} at "{}:{}" as consumer in {} group succeeded.'.format(
                    topic,
                    server,
                    port,
                    group
                )
                logger.info(message)
                break
            except NoBrokersAvailable as e:
                if tries == 5:
                    message = 'Connection to topic {} at "{}:{}" as consumer in {} group failed.'.format(
                        topic,
                        server,
                        port,
                        group
                    )
                    logger.error(message)
                    raise e
                else:
                    time.sleep(2)
                    # pass
            tries += 1

    def connect_out(self, server, topic, port=9092):
        self.kafka_out.connect(
            server=server,
            topic=topic,
            port=port
        )

    def ready(self):
        rdy = self.kafka_in.ready() and self.kafka_out.ready()

        logger.debug('Ready: ' + str(rdy))
        return rdy

    def pull(self, offset=None):
        return self.kafka_in.pull(offset)

    def push(self, message):
        self.kafka_out.push(message)
