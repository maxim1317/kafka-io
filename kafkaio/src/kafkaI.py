from .utils import setup_logger, check_ip


logger = setup_logger()


class KafkaIn(object):
    """Sets consumer Kafka connection"""

    def __init__(self):
        self._consumer = None
        pass

    def ready(self):
        return self._consumer is not None

    def connect(
        self,
        server,
        topic,
        group,
        port=9092
    ):
        self._ip    = server
        self._port  = port
        self._topic = topic
        self._group = group

        self._address = check_ip(self._ip) + ':' + str(self._port)

        self._connect()

    def _connect(self):
        from kafka import KafkaConsumer
        from json import loads

        if self._consumer is not None:
            self._consumer.close()

        try:
            self._consumer = KafkaConsumer(
                # self._topic,
                bootstrap_servers=[self._address],
                # auto_offset_reset='latest',
                enable_auto_commit=True,
                # group_id=self._group,
                value_deserializer=lambda x: loads(x.decode('utf-8'))
            )

            self._assign_topic()

            return

        except Exception as e:
            logger.error(str(e))
            raise e

    def _assign_topic(self):
        from kafka import TopicPartition

        self.partition = TopicPartition(self._topic, 0)
        assigned_topic = [self.partition]
        self._consumer.assign(assigned_topic)

        return

    def _move_to_end(self):
        self._consumer.seek_to_end(self.mypartition)

    def _move_to_offset(self, offset):
        self._consumer.seek(self.mypartition, offset)

    def pull(
        self,
        offset=None
    ):
        if self._consumer is None:
            raise Exception('No connection. Use KafkaIn.connect() to create one.')

        if offset is None:
            self._move_to_end()
        else:
            self._move_to_offset(offset)

        for message in self._consumer:
            return message


