from .utils import setup_logger, check_ip


logger = setup_logger()


class KafkaOut(object):
    """Sets producer Kafka connection"""

    def __init__(self):
        self._producer = None
        pass

    def ready(self):
        return self._producer is not None

    def connect(
        self,
        server,
        topic,
        port=9092
    ):
        self._ip    = server
        self._port  = port
        self._topic = topic

        self._address = check_ip(self._ip) + ':' + str(self._port)

        self._connect()

    def _connect(self):
        from kafka import KafkaProducer
        from json import dumps

        if self._producer is not None:
            self._producer.close()

        try:
            self._producer = KafkaProducer(
                bootstrap_servers=[self._address],
                value_serializer=lambda x: dumps(x.decode('utf-8'))
            )

            return

        except Exception as e:
            raise e

    def _check_moc(self, message):  # TODO
        """Validates message

        Args:
            message (dict): Message to send

        Returns:
            Nothing

        Raises:
            ve: jsonschema.exceptions.ValidationError
        """
        from jsonschema import validate
        from jsonschema.exceptions import ValidationError

        #######
        return
        #######

        schema = {
            "type" : "object",
            "properties" : {
                "price" : {"type" : "number"},
                "name" : {"type" : "string"},
            },
        }

        try:
            validate(instance=message, schema=schema)
            return
        except ValidationError as ve:
            raise ve

    def push(self, message):
        if self._producer is None:
            raise Exception('No connection. Use KafkaIn.connect() to create one.')

        try:
            self._check_moc()
            self._producer.send(topic=self._topic, value=message)

            return
        except Exception as e:
            raise e
