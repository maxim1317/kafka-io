def check_ip(ip):
    import ipaddress

    ip_exception = None

    try:
        ipaddress.ip_address(ip)
    except ValueError as e:
        ip_exception = e

    ip = ip.split(':')[0]

    try:
        ipaddress.ip_address(ip)
    except ValueError:
        raise ip_exception

    return ip


def setup_logger():
    """Return a logger with a default ColoredFormatter."""
    import logging
    from colorlog import ColoredFormatter

    formatter = ColoredFormatter(
        "%(log_color)s%(levelname)-8s%(reset)s %(blue)s%(message)s",
        datefmt=None,
        reset=True,
        log_colors={
            'DEBUG':    'cyan',
            'INFO':     'green',
            'WARNING':  'yellow',
            'ERROR':    'red',
            'CRITICAL': 'red',
        }
    )

    logger = logging.getLogger('kafkaIO')
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

    return logger
